package main

import (
	"fmt"
	"math"
)

func main() {
	credit_sum_diff(120000000.00, 1.0, 5.0)
	// credit_sum_ann(120000000.00, 1.0, 5.0)
}

func credit_sum_ann(amount, period, rate float64) {
	var num int = int(period) * 12
	var i float64 = rate / (12 * 100)
	var n float64 = float64(period) * 12
	k := (i * math.Pow((1.0+i), n)) / (math.Pow((1.0+i), n) - 1.0)
	cnst := amount * k

	for i := 1; i <= num; i++ {
		f := (rate * amount) / (12 * 100)
		asQ := cnst - f
		fmt.Printf("Kredit balansi %.1f Asosiy qarz %.1f Foiz %.1f Jami oylik to`lovi %.1f\n", amount, asQ, f, cnst)
		amount -= asQ
	}
}

func credit_sum_diff(amount, period, rate float64) {
	var num int = int(period) * 12
	cnst := amount / (12.0 * period)
	for i := 1; i <= num; i++ {
		f := (rate * amount) / (12 * 100)
		sum := cnst + f
		fmt.Printf("Kredit balansi %.1f Asosiy qarz %.1f Foiz %.1f Jami oylik to`lovi %.1f\n", amount, cnst, f, sum)
		amount -= cnst
	}
}
