package main

import (
	"fmt"
	"math"
)

func main() {

	fmt.Println(MySquareRoot(2, 4))
}

func MySquareRoot(num, precision uint) (result float64) {
	left := 1.0
	right := float64(num)
	var pow = math.Pow(10, float64(precision))
	var prec = 1 / (math.Pow(10, float64(precision)))

	for left <= right {
		mid := (left + right) / 2

		if math.Abs(mid*mid-float64(num)) <= prec {
			return float64(math.Round(mid*pow)) / pow
		} else if mid*mid > float64(num) {
			right = mid
		} else {
			left = mid
		}
	}
	return
}
